# web-testing
experiments with app testing using Espresso 
//TODO - add build status and logs from travis/gitlab

# How I approached it

1. I started with a [plan](./Docs/Tasks_plan.md) to estimate types of tasks and how much time I need to spend on them
2. Than I created [high-level checklist](./Docs/Checklist.md#high-level-checks) of elements that presented on [SUT](https://en.wikipedia.org/wiki/System_under_test). This is important for me to visualise scope of work and have model of future tests
3. Gradually I extended check-list with [more detailed checks](./Docs/Checklist.md#elements) (edge cases, negative tests)
4. After that I created [high-level test scenarios](./Docs/Checklist.md#test-scenarios), which will be *Smoke* and *Extended* test suites.
5. I did quick research and selected tool, language and framework for future tests. [Notes on reasoning for selection](./Docs/Selection_of_tools.md)
6. Than I setup [environment for development](./Docs/Environment_setup.md) 
7. Than I setup [tests framework ](./Docs/Framework_setup.md)
8. Than I designed how test structure will look like with basic [PageObjects](./tests/PageObjects) and [Test cases](./tests/TestCases)
9. Than I [Setup CI](./Docs/Environment_setup.md##setup-ci) (GitLAb CI and Sauselabs)

[Links used in process](./Docs/Links.md)

# How to run tests:
1. Clone this repository
2. Check that you have android device/emulator connected
3. Navigate to Calc folder and execute ./gradlew clean unistallAll spoon

Tests also supposed to be executed on CI but GitLab runners do not support kvm, so only ARM anroid emulators can be working. This is painfull and slow.

Thats why I setup integration with device cloud - bitbar.com.
Results of test runs are downloaded as artifacts here:
To watch on results you can login using this credentials:

https://cloud.testdroid.com

login:cewoduca@pavilionx2.com

pass:cewoduca

Link to project: https://cloud.testdroid.com/#service/projects/121103452