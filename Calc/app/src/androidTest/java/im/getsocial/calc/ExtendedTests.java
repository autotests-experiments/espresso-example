package im.getsocial.calc;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigInteger;

@RunWith(AndroidJUnit4.class)
public class ExtendedTests {


    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testDoubleAddition() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        double arg1 = 1.5;
        double arg2 = 2.5;
        calc.addition(arg1,arg2);
        calc.checkAddition(arg1,arg2);
    }
    public void testDoubleSubtraction() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        double arg1 = 1.5;
        double arg2 = 2.5;
        calc.subtraction(arg1,arg2);
        calc.checkSubtraction(arg1,arg2);
    }
    public void testDoubleMultiplication() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        double arg1 = 1.5;
        double arg2 = 2.5;
        calc.multiplication(arg1,arg2);
        calc.checkMultiplication(arg1,arg2);
    }
    public void testDoubleDivision() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        double arg1 = 1.5;
        double arg2 = 2.5;
        calc.division(arg1,arg2);
        calc.checkDivision(arg1,arg2);
    }

    @Test
    public void testEmptyValues() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        calc.clickAddition();
        calc.checkResult("");
        calc.clickSubtraction();
        calc.checkResult("");
        calc.clickMultiplication();
        calc.checkResult("");
        calc.clickDivision();
        calc.checkResult("");
    }

    @Test
    public void testOneEmptyValue() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        long arg1 = 6;
        calc.setArg1(arg1);
        testEmptyValues();
    }
    @Test
    public void testOneZeroValue() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        long arg1 = 6;
        long arg2 = 0;
        calc.addition(arg1,arg2);
        calc.checkAddition(arg1,arg2);
        calc.subtraction(arg1,arg2);
        calc.checkSubtraction(arg1,arg2);
        calc.multiplication(arg1,arg2);
        calc.checkMultiplication(arg1,arg2);
        calc.division(arg1,arg2);
        calc.checkDivision(arg1,arg2);
    }

    @Test
    public void testNegativeAddition() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        long arg1 = -6;
        long arg2 = 2;
        calc.addition(arg1, arg2);
        calc.checkAddition(arg1, arg2);
        arg2 = -2;
        calc.addition(arg1, arg2);
        calc.checkAddition(arg1, arg2);
    }
    @Test
    public void testNegativeSubtraction() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        long arg1 = -6;
        long arg2 = 2;
        calc.subtraction(arg1, arg2);
        calc.checkSubtraction(arg1, arg2);
        arg2 = -2;
        calc.subtraction(arg1, arg2);
        calc.checkSubtraction(arg1, arg2);
    }
    @Test
    public void testNegativeMultiplication() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        long arg1 = -6;
        long arg2 = 2;
        calc.multiplication(arg1, arg2);
        calc.checkMultiplication(arg1, arg2);
        arg2 = -2;
        calc.multiplication(arg1, arg2);
        calc.checkMultiplication(arg1, arg2);
    }

    @Test
    public void testNegativeDivision() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        long arg1 = -6;
        long arg2 = 2;
        calc.division(arg1, arg2);
        calc.checkDivision(arg1, arg2);
        arg2 = -2;
        calc.division(arg1, arg2);
        calc.checkDivision(arg1, arg2);
    }

    @Test
    public void testNotNum() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        String arg1 = "string";
        String arg2 = "another string";
        calc.setArg1WithString(arg1);
        calc.checkArg1WithString("");
        calc.setArg2WithString(arg2);
        calc.checkArg2WithString("");
    }
    @Test
    public void testMaxLong() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        // max long value is 9223372036854775807, so lets get bigger one
        String arg1 = "9223372036854775808";
        String arg2 = "10";
        calc.setArg1WithString(arg1);
        calc.checkArg2WithString(arg1);
        BigInteger big1 = new BigInteger(arg1);
        BigInteger big2 = new BigInteger(arg2);
        BigInteger bigResult;
        bigResult = big1.add(big2);
        calc.clickAddition();
        calc.checkResult(bigResult.toString());
        calc.clickSubtraction();
        bigResult = big1.subtract(big2);
        calc.checkResult(bigResult.toString());
        calc.clickMultiplication();
        bigResult = big1.multiply(big2);
        calc.checkResult(bigResult.toString());
        calc.clickDivision();
        bigResult = big1.divide(big2);
        calc.checkResult(bigResult.toString());
    }
}
