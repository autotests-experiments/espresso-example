package im.getsocial.calc;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class SmokeTests {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testIntAddition() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        long arg1 = 3;
        long arg2 = 2;
        calc.addition(arg1, arg2);
        calc.checkAddition(arg1, arg2);
    }

    @Test
    public void testIntSubtraction() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        long arg1 = 3;
        long arg2 = 1;
        calc.subtraction(arg1, arg2);
        calc.checkSubtraction(arg1, arg2);
    }

    @Test
    public void testIntMultiplication() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        long arg1 = 3;
        long arg2 = 2;
        calc.multiplication(arg1, arg2);
        calc.checkMultiplication(arg1, arg2);
    }

    @Test
    public void testIntDivision() throws InterruptedException {
        CalcPageObject calc = new CalcPageObject();
        long arg1 = 6;
        long arg2 = 3;
        calc.division(arg1, arg2);
        calc.checkDivision(arg1, arg2);
    }

}