package im.getsocial.calc;

import android.view.View;

import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

public class CalcPageObject {
    private Matcher<View> result = withContentDescription("result");
    private Matcher<View> header = withText("Calc");
    private Matcher<View> arg1Field = withContentDescription("arg1");
    private Matcher<View> arg2Field = withContentDescription("arg2");
    private Matcher<View> addition = withContentDescription("addition");
    private Matcher<View> subtraction = withContentDescription("subtraction");
    private Matcher<View> multiplication = withContentDescription("multiplication");
    private Matcher<View> division = withContentDescription("division");

    public CalcPageObject() {
        onView(header).check(matches(isDisplayed()));
        onView(arg1Field).check(matches(isDisplayed()));
        onView(arg2Field).check(matches(isDisplayed()));
        onView(addition).check(matches(isDisplayed()));
        onView(subtraction).check(matches(isDisplayed()));
        onView(multiplication).check(matches(isDisplayed()));
        onView(division).check(matches(isDisplayed()));
        onView(result).check(matches(isDisplayed()));
    }
    private void setArgWithString(Matcher<View> argField, String argString) {
        onView(argField).perform(typeText(argString));
    }
    private void checkArgWithString(Matcher<View> argField, String argString) {
        onView(allOf(withText(argString), argField)).check(matches(isDisplayed()));
    }
    public void setArg1WithString(String argString) {
        setArgWithString(arg1Field,argString);
    }
    public void setArg2WithString(String argString) {
        setArgWithString(arg2Field,argString);
    }
    public void checkArg1WithString(String argString) {
        checkArgWithString(arg1Field,argString);
    }
    public void checkArg2WithString(String argString) {
        checkArgWithString(arg2Field,argString);
    }

    public void setArg1(double arg) {
        String argStr = Double.toString(arg);
        onView(arg1Field).perform(clearText());
        setArgWithString(arg1Field, argStr);
        checkArgWithString(arg1Field,argStr);
    }
    public void setArg1(long arg) {
        String argStr = Long.toString(arg);
        onView(arg1Field).perform(clearText());
        setArgWithString(arg1Field, argStr);
        checkArgWithString(arg1Field,argStr);
    }
    public void setArg2(double arg) {
        String argStr = Double.toString(arg);
        onView(arg2Field).perform(clearText());
        setArgWithString(arg2Field, argStr);
        checkArgWithString(arg2Field,argStr);
    }
    public void setArg2(long arg) {
        String argStr = Long.toString(arg);
        onView(arg2Field).perform(clearText());
        setArgWithString(arg2Field, argStr);
        checkArgWithString(arg2Field,argStr);
    }
    public void clickAddition(){
        onView(addition).perform(click());
    }
    public void clickSubtraction(){
        onView(subtraction).perform(click());
    }
    public void clickMultiplication(){
        onView(multiplication).perform(click());
    }
    public void clickDivision(){
        onView(division).perform(click());
    }
    public void checkResult(String expectedText){
        onView(result).check(matches(withText(expectedText)));
    }

    public void addition(double arg1, double arg2) throws InterruptedException {
        setArg1(arg1);
        setArg2(arg2);
        clickAddition();
    }
    public void addition(long arg1, long arg2) throws InterruptedException {
        setArg1(arg1);
        setArg2(arg2);
        clickAddition();
    }
    public void checkAddition(double arg1, double arg2){
        double trueResult = arg1 + arg2;
        String trueResultStr = Double.toString(trueResult);
        onView(allOf(result, withText(trueResultStr))).check(matches(isDisplayed()));
    }
    public void checkAddition(long arg1, long arg2){
        long trueResult = arg1 + arg2;
        String trueResultStr = Long.toString(trueResult);
        onView(allOf(result, withText(trueResultStr))).check(matches(isDisplayed()));
    }
    public void subtraction(double arg1, double arg2) throws InterruptedException {
        setArg1(arg1);
        setArg2(arg2);
        onView(subtraction).perform(click());
    }
    public void subtraction(long arg1, long arg2) throws InterruptedException {
        setArg1(arg1);
        setArg2(arg2);
        onView(subtraction).perform(click());
    }
    public void checkSubtraction(double arg1, double arg2){
        double trueResult = arg1 - arg2;
        String trueResultStr = Double.toString(trueResult);
        onView(allOf(result, withText(trueResultStr))).check(matches(isDisplayed()));
    }
    public void checkSubtraction(long arg1, long arg2){
        long trueResult = arg1 - arg2;
        String trueResultStr = Long.toString(trueResult);
        onView(allOf(result, withText(trueResultStr))).check(matches(isDisplayed()));
    }
    public void multiplication(double arg1, double arg2) throws InterruptedException {
        setArg1(arg1);
        setArg2(arg2);
        onView(multiplication).perform(click());
    }
    public void multiplication(long arg1, long arg2) throws InterruptedException {
        setArg1(arg1);
        setArg2(arg2);
        onView(multiplication).perform(click());
    }
    public void checkMultiplication(double arg1, double arg2){
        double trueResult = arg1 * arg2;
        String trueResultStr = Double.toString(trueResult);
        onView(allOf(result, withText(trueResultStr))).check(matches(isDisplayed()));

    }
    public void checkMultiplication(long arg1, long arg2){
        long trueResult = arg1 * arg2;
        String trueResultStr = Long.toString(trueResult);
        onView(allOf(result, withText(trueResultStr))).check(matches(isDisplayed()));
    }
    public void division(double arg1, double arg2) throws InterruptedException {
        setArg1(arg1);
        setArg2(arg2);
        onView(division).perform(click());
    }
    public void division(long arg1, long arg2) throws InterruptedException {
        setArg1(arg1);
        setArg2(arg2);
        onView(division).perform(click());
    }
    public void checkDivision(double arg1, double arg2){
        if (arg2 != 0) {
            double trueResult = arg1 / arg2;
            String trueResultStr = Double.toString(trueResult);
            onView(allOf(result, withText(trueResultStr))).check(matches(isDisplayed()));
        }
        else {
            onView(allOf(result, withText("You cannot divide by 0 unless your name is Chuck"))).check(matches(isDisplayed()));
        }
            }
    public void checkDivision(long arg1, long arg2){
        if (arg2 != 0) {
            long trueResult = arg1 / arg2;
        String trueResultStr = Long.toString(trueResult);
        onView(allOf(result, withText(trueResultStr))).check(matches(isDisplayed()));
        }
        else {
            onView(allOf(result, withText("You cannot divide by 0 unless your name is Chuck"))).check(matches(isDisplayed()));
        }
    }
}