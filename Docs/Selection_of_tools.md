### Selection of tools

## Selection of test automation tools

*Summary:* I decided to work on Espresso tests with Page Object pathern, another approach that I considered was Robot pattern with Espresso + Kotlin

There is not much choise for modern app test automation. 
# Just to give some perspective on tools and meththods:
1. UI-based tools:
    - [Sikuli](http://sikulix.com/#home2), fun to play with but not for apps
    - [MonkeyTalk] (https://www.oracle.com/search/results?Nty=1&Ntk=S3&Ntt=cloudMonkey) - looks like not active in last years
    - [Ranorex](https://www.ranorex.com/) - too much a silver bullet
2. Testing Frameworks:
    - [Calabash](http://calaba.sh/) - [going to be discontinued soon](http://automated-testing.info/t/calabash-ios-and-calabash-android-last-news/13503)
    - [Appium](http://appium.io/) - have it's procs and cons
    - AAAnd finally [Espresso](https://google.github.io/android-testing-support-library/docs/espresso/)

I selected Espresso, it's native framework for Android test automation provided by Google. This choise is closest to what devs will like/support and it's important because dev help is often needed to resolve any blockers on the way of QA.
I also personally think that testing is part dev process and most effitiently  tests can be writen(coded) by devs, and QA here to help with priorities, proceses, infra and test scripts

# Automation methods/paterns:

1. Record-and-playback: not reliable, not scalable, not easlity maintainable. Fast
2. Linear scripting: a bit of code, bit more reliable that record-and-plaback but other problems are there
3. BDD ([Cucumber,JBehave etc](https://dzone.com/articles/brief-comparison-bdd)). Suppose to work in agile teams, gaive understanding and visibility of tests to all team (PO can even create user stories in this DSL and they can be converted to tests on fly). For developers imho its adds another layer that needs to be supported.
4. Modular tests:
- [Page Objects](https://martinfowler.com/bliki/PageObject.html) as example. Reusable components, clear code, better maintainability
- [Robot pattern](https://news.realm.io/news/kau-jake-wharton-testing-robots/), looks similar to Page Objects, will be interesting to try one time
5. [Combinations of previous methods, f.e.](https://github.com/sebaslogen/CleanGUITestArchitecture) - this one looks interesting but I usually think that extra layer of DSL is not much helping
I choose Modular PageObjects tests, I beleive that tests should be easy to understand, write and extend. 

# Language: 

1. Java. Native language
2. Python. Fast to start, easy to read, dinamic
3. JS. Standart for web development (so most likely be easy to extend tests if you already have web project and web QA)

I choose Java. I beleive it's fastest way to get started.

# Other tools:
1. [Spoon](http://square.github.io/spoon/) - orchestrator of tests and test result reporter. Generates clean and readable results with screenshots.
2. [Spoon gradle plugin](https://github.com/stanfy/spoon-gradle-plugin) - integrates spoon runner in gradle tasks
3. If I will have time, I will try to migrate from spoon to [fork](https://github.com/shazam/fork) :) Sharding of tests + flakynnes report + performance report looks like great things

# Code and docs organization:
I choose gitlab, I wanted to experiment on this concept of repository + CodeReview tool + CI + task/issue tracker as one service.
Bitbucket is also great option, as it offers free private repost, maybe I will look on it later.

Docs will live in same repo, so it's easier to track when they are changed.  Gitlab provides UI for markdown so my docs will be in markdown format. And also you can edit files in Web, which is great if you need to fix some typos

# CI:
I was thinking of setting up personal CI server (Jenkins) in my [Digital Ocean ](https://www.digitalocean.com/) droplert. In the end I decided to go with cloud solution that Gitlab provides. It suppose to be faster to setup and also have some integration with test clouds and notification channel of your choise(Slack mostlikely)

# Test Executors
1. Locally tests were created and debugged on emulator with api21.
2. On GitLab test can be also executed on emulators inside Docker containers, need bit research on this, but looks like only arm images are supported because for x86 you need to have KVM/VT-d enabled 
3. As test lab I will use [Sauselabs](https://saucelabs.com), 90h of runtime and 14days of trial period should be enought to try it out.
If not, I will go with [BitBar](bitbar.com)