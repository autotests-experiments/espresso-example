# Checklist for Calc app

*Bugs*:
1. crash on devision by 0
2. cannot operate non-int numbers
3. cannot operate negative digits
4. crash if operate with empty argument field (any) 
5. round operation (devision) result (3/2 =1. 2/3 = 0), same for results
6. negative result on multiplication of big numbers (100000000*100000000 = -84463....)
7. App state not persists on rotation

*Notes*: Checklist for Calc app

## High level checks
App is launched
content is present and in default state:
    1. App Header
    2. First Argument field
    3. Second Argument field
    4. math operation buttons
    5. Result (blank)
User focus in First argument field

## Elements:
### First argument field:
1. Active by default
2. Have placeholder text 
3. Num Keyboard is shown by tap
4. User can enter and delete text
5. *secondary checks*:
- copy/paste
- paste non-numberical
- enter non-int numbers (1.2 f.e.)
- enter negative numbers (-2 f.e)
- enter 0
- enter max.value

### Second argument field:
1. Same as first argument field but not active by default

### + button:
1. actually do math correctly
2. working it one of argument is negative
3. working if one of argument is decimal
4. works correctly with big numbers
5. result can be negative numbers


### - button:
1. Same as for "+ button"

### * button:
1. same as for "+ button"

### / button:
1. same as for "+ button"
2. cannot divide by 0

### Result text label
1. have placeholder "Result" by default
2. contains correct values of data

## Smoke Test:
1. [x] check that all elements are present
2. [x] check that you can perform basic operation with valid input

## Extended Test:
1. [x] check execution of operations with empty value
2. [x] check when one of arguments with 0
3. [x] check [devision by 0](https://s-media-cache-ak0.pinimg.com/originals/19/f9/63/19f9633fd936ba5909deee98e16b4c51.jpg)
3. [x] check with decimals
4. [x] check with not-numbers
5. [x] check with negative digits
6. [x] check with big numbers