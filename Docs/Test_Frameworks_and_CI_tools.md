*Test Frameworks*
*iOS:*
1. XCUITest
*Android:*
1.Expresso
*Tools*:
1. [spoon](http://square.github.io/spoon/) for representation of test results and orkestration of tests on test executors.
2. [spoon-gradle-plugin](https://github.com/stanfy/spoon-gradle-plugin)

*Tests approach*:
1. [Page Objects](https://github.com/googlesamples/android-architecture/issues/225)
2. [Robots](https://news.realm.io/news/kau-jake-wharton-testing-robots/)

*CI/CD tools*
1. [nevercode](https://nevercode.io/index.html#product) -> I choose it as test executor
2. [Sauselabs](https://saucelabs.com/enterprise#mobile-testing)
3. [GitLab](https://about.gitlab.com/) -> I choose it as build and test runner

*Doc format*
I selected MD as format for documentation. It's easily readable for humans and can be displayed nicely

Also MD can be converted to html and displayed as static page, see more on [gitlab pages](https://about.gitlab.com/2016/04/04/gitlab-pages-get-started/)


*Issues*:
compatibility issue between main and dest apk:
https://sites.google.com/a/android.com/tools/tech-docs/new-build-system/user-guide#TOC-Resolving-conflicts-between-main-and-test-APK
solution (quick): 
```gradle
configurations.all {
    resolutionStrategy.force 'com.android.support:support-annotations:22.2.0'
}
```
solution (rigth): setup excludes for test apk

lots of issues with gradle, looks like fixed if update gradle to newer version
        classpath 'com.android.tools.build:gradle:2.2.2'
and updating distributionURL in gradle-wrapper file
distributionUrl=https\://services.gradle.org/distributions/gradle-2.14.1-all.zip

*notes*:
maybe I should have used this http://stackoverflow.com/questions/2186931/java-pass-method-as-parameter for assertion, so I can make function like checkTrueValues(arg1,arg2,function());

