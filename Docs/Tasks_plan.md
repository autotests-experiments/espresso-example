## Plan of tasks
1. [x] Review assignment, 10min
2. [x] Get source code and skim thrue, 10min
3. [x] Setup IDE for project and try to build it. Resolve possible issues [20min]
3. [x] Sheck possible way to automate testng: Espresso or Appium,? 10min
4. [x] write map of automaton 30min
    1. [x] Pages: looks like it's only one there, so not sure page objec20is ]applicable :)
    2. [x] Setup and tear down requirements
    3. [x] Write positive tests checklist
    4. [x] Write negative tests checklist
5. [ ] Setup automation environment and tools, document it [20min]
 Create private git repo with test code, think about sctucture (master + Experimental branches) [20min]
6. [ ] Write tests and debug them 2h
7. [ ] prepare infrastructure to run tests (Jenkins or other CI for building app and tests, test orkestrathor, test executors: emulators or reald devices on test clouds) 1h
8. [ ] setup CI to trigger tests (digital ocean for hosting?) 1h

Time for write autmated test cases = 2h
TOTAL TIME = 6-8h